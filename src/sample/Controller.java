package sample;

import javafx.event.Event;
import javafx.fxml.FXML;

import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


import java.io.*;
import java.util.Optional;


public class Controller{
    @FXML
    TextArea tArea;

    @FXML
    Button btnCortar;

    @FXML
    Button btnCopiar;

    @FXML
    Button btnPegar;

    @FXML
    ToggleGroup fuente;

    @FXML
    ToggleGroup tamano;

    @FXML
    MenuItem mn;

    //private Stage stage;

    final Clipboard clipboard = Clipboard.getSystemClipboard();
    final ClipboardContent content = new ClipboardContent();


    public void initialize() {
        btnCopiar.setDisable(true);
        btnPegar.setDisable(true);
    }

    public boolean shutdown() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Salir");
        alert.setContentText("Quieres salir?");
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }


    public void onClickArchivo(Event event) throws IOException {
        mn = (MenuItem) event.getSource();
        String opcion = mn.getText();
        //Stage stage;
        if (opcion.equals("Guardar")) {
            FileChooser fileChooser = new FileChooser();
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showSaveDialog(null);
            if (file != null) {
                saveFile(tArea.getText(), file);
            }
        } else if (opcion.equals("Abrir")) {
            FileChooser fileChooser = new FileChooser();
            File file = fileChooser.showOpenDialog(new Stage());
            if (file.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String st;
                tArea.setText("");
                while ((st = br.readLine()) != null) {
                    tArea.appendText(st + "\n");
                }
                showCopyPaste(tArea.getText());
                Stage stg = (Stage)tArea.getScene().getWindow();
                stg.setTitle(file.getName());
            }
        } else {
            if (shutdown()) Platform.exit();
        }
    }

    public void saveFile(String text, File file) {
        System.out.println("oook");
        try {
            FileWriter fileWriter;
            fileWriter = new FileWriter(file);
            fileWriter.write(text);
            fileWriter.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Save");
            alert.setHeaderText(null);
            alert.setContentText("Documento guardado!");
            alert.showAndWait();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void onClickOpciones() {
        tArea.setFont(Font.font(getFuente(), getTamano()));
    }

    public double getTamano() {
        RadioMenuItem rmt = (RadioMenuItem) tamano.getSelectedToggle();
        String tamano = rmt.getText();
        switch (tamano) {
            case "Pequeño":
                return 12;
            case "Medio":
                return 20;
            case "Grande":
                return 30;
        }
        return 0;
    }

    public String getFuente() {
        RadioMenuItem rmf = (RadioMenuItem) fuente.getSelectedToggle();
        return rmf.getText();
    }

    public void onClickAyuda() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Sobre el editor");
        alert.setHeaderText("JAVA FX EDITOR");
        alert.setContentText("Java Fx\nM07 Desenvolupament d'interfícies\nJenny Licini 2 DAM");
        alert.showAndWait();
    }

    public void onClickCortar() {
        tArea.replaceSelection("");
    }

    public void onClickCopiar() {
        String myText = tArea.getSelectedText();
        content.putString(myText);
        clipboard.setContent(content);
    }

    public void onClickPegar() {
        int caretPosition = tArea.getCaretPosition();
        tArea.insertText(caretPosition, clipboard.getString());
    }

    public void keyReleasedProperty(KeyEvent event) {
        tArea = (TextArea) event.getSource();
        showCopyPaste(tArea.getText());
    }


    public void showCopyPaste(String txt) {
        boolean isDisabled = (txt.isEmpty());
        btnCopiar.setDisable(isDisabled);
        btnPegar.setDisable(isDisabled);
    }
}