package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.util.concurrent.ExecutorService;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent root = loader.load();
        Controller controller=loader.getController();
        primaryStage.setTitle("Editor de Texto");
        primaryStage.setScene(new Scene(root, 428, 459));
        primaryStage.setResizable(false);

        primaryStage.setOnCloseRequest(event -> {
            if (!controller.shutdown()) {
                event.consume();
            }
        });

        primaryStage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }
}
